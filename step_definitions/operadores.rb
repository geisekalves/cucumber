Dado('que eu compre {int} caixas de uva') do |quantidade|
    @cxUva = quantidade
    puts @cxUva
end
    
Quando('eu vendo {int} caixas para meu primo Rodolfo') do |qtdVendida|
    @cxVendida = qtdVendida
    @total = @cxUva - @cxVendida
    puts @total
end
    
Então('quantas caixas sobram') do
    expect(@total) .to eq 7
end

Então('deve sobrar apenas {int} caixas de uva') do |qtdSobra|
    expect(@total).to eq qtdSobra
end